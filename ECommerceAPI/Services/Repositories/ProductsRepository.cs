﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using ECommerceAPI.Data;
using ECommerceAPI.DTOs.Request.Product;
using ECommerceAPI.DTOs.Response;
using ECommerceAPI.Entities;
using ECommerceAPI.Enums;
using ECommerceAPI.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace ECommerceAPI.Services.Repositories
{
    /// <summary>
    /// <see cref="IProductsRepository"/>
    /// </summary>
    public class ProductsRepository : IProductsRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public ProductsRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <inheritdoc/>
        public async Task AddNewProductAsync(NewProductDto newProductDto, Category category)
        {
            var product = new Product
            {
                Name = newProductDto.Name,
                Price = newProductDto.Price,
                Description = newProductDto.Description,
                ProductId = $"{category.Codename}_{newProductDto.GetHashCode()}",
                ProductCategory = category
            };

            await _context.Products.AddAsync(product);
        }

        /// <inheritdoc/>
        public async Task<bool> CheckProductNameExistsAsync(string productName)
        {
            var check = await _context.Products.FirstOrDefaultAsync(prod => prod.Name.ToLower() == productName.ToLower());

            // No Product with that name
            if (check == null)
                return true;

            return false;
        }

        /// <inheritdoc/>
        public async Task<List<ProductResponse>> CategoryProductsAsync(GetCategoryProductsDto getCategoryProductsDto)
        {
            var products = _context.Products.Where(prod => prod.ProductCategory.Name == getCategoryProductsDto.CategoryName);

            switch(getCategoryProductsDto.SortOrder)
            {
                case SortOrder.Ascending:
                    products = products.OrderBy(prods => prods.Name);
                    break;
                case SortOrder.Descending:
                    products = products.OrderByDescending(prods => prods.Name);
                    break;
                case SortOrder.LowToHigh:
                    products = products.OrderBy(prods => prods.Price);
                    break;
                case SortOrder.HighToLow:
                    products = products.OrderByDescending(prods => prods.Price);
                    break;
            }

            var productList = await products.ProjectTo<ProductResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return productList;
        }
    }
}
