﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using ECommerceAPI.Data;
using ECommerceAPI.DTOs.Request.Category;
using ECommerceAPI.DTOs.Response;
using ECommerceAPI.Entities;
using ECommerceAPI.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace ECommerceAPI.Services.Repositories
{
    /// <summary>
    /// <see cref="ICategoriesRepository"/>
    /// </summary>
    public class CategoriesRepository : ICategoriesRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public CategoriesRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <inheritdoc/>
        public async Task AddNewCategoryAsync(NewCategoryDto newCategoryDto)
        {
            var category = new Category
            {
                Name = newCategoryDto.Name,
                Codename = newCategoryDto.Codename,
            };

            await _context.Categories.AddAsync(category);
        }

        /// <inheritdoc/>
        public async Task<bool> CheckCategoryNameExistsAsync(string categoryName)
        {
            var check = await _context.Categories.FirstOrDefaultAsync(cat => cat.Name.ToLower() == categoryName.ToLower());

            // No Category with that name
            if (check == null)
                return true;

            return false;
        }

        /// <inheritdoc/>
        public async Task<List<CategoryResponse>> GetAllCategoriesAsync()
        {
            var categories = await _context.Categories.ProjectTo<CategoryResponse>(_mapper.ConfigurationProvider).ToListAsync();

            return categories;
        }

        /// <inheritdoc/>
        public async Task<Category> GetCategoryByNameAsync(string categoryName)
        {
            var category = await _context.Categories.FirstOrDefaultAsync(cat => cat.Name == categoryName);

            return category;
        }
    }
}
