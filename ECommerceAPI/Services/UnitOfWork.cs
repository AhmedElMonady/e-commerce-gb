﻿using AutoMapper;
using ECommerceAPI.Data;
using ECommerceAPI.Interfaces;
using ECommerceAPI.Services.Repositories;

namespace ECommerceAPI.Services
{
    /// <summary>
    /// <see cref="IUnitOfWork"/>
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public UnitOfWork(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <inheritdoc/>
        public ICategoriesRepository CategoriesRepository => new CategoriesRepository(_context, _mapper);

        /// <inheritdoc/>
        public IProductsRepository ProductsRepository => new ProductsRepository(_context, _mapper);

        /// <inheritdoc/>
        public async Task<bool> CheckSaveStatusAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
