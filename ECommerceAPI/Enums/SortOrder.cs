﻿namespace ECommerceAPI.Enums
{
    /// <summary>
    /// Sorting options
    /// </summary>
    public enum SortOrder
    {
        /// <summary>
        /// Default order
        /// </summary>
        None,
        /// <summary>
        /// Ascending order by Name
        /// </summary>
        Ascending,
        /// <summary>
        /// Descending order by Name
        /// </summary>
        Descending,
        /// <summary>
        /// Descending order by Price
        /// </summary>
        HighToLow,
        /// <summary>
        /// Ascending order by Price
        /// </summary>
        LowToHigh
    }
}
