﻿namespace ECommerceAPI.DTOs.Response
{
    /// <summary>
    /// Represents the Category response that is sent to the client as a JSON response.
    /// </summary>
    public class CategoryResponse
    {
        /// <summary>
        /// Unique Category ID in the database.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Category name in the database.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Category codename in the database.
        /// </summary>
        public string Codename { get; set; }
    }
}
