﻿namespace ECommerceAPI.DTOs.Response
{
    /// <summary>
    /// Represents the Product response that is sent to the client as a JSON response.
    /// </summary>
    public class ProductResponse
    {
        /// <summary>
        /// Unique Product ID in the database.
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// Product Item Name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Price to be displayed.
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// A brief description of the product.
        /// </summary>
        public string Description { get; set; }
    }
}
