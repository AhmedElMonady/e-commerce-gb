﻿using ECommerceAPI.Enums;

namespace ECommerceAPI.DTOs.Request.Product
{
    /// <summary>
    /// Represents a request object for retrieving Category related Products.
    /// </summary>
    public class GetCategoryProductsDto
    {
        /// <summary>
        /// Category name that its Products will be retrieved.
        /// </summary>
        public string CategoryName { get; set; }
        /// <summary>
        /// Order which the Products will be represented.
        /// </summary>
        public SortOrder SortOrder { get; set; } = SortOrder.None;
    }
}
