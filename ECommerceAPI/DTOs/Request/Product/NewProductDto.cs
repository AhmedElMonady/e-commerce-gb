﻿namespace ECommerceAPI.DTOs.Request.Product
{
    /// <summary>
    /// Represents a request object for creating new Category.
    /// </summary>
    public class NewProductDto
    {
        /// <summary>
        /// Product name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Product price.
        /// </summary>
        public double Price { get; set; }
        /// <summary>
        /// Product description.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Represents the Category name that will be linked to.
        /// </summary>
        public string CategoryName { get; set; }
    }
}
