﻿using System.ComponentModel.DataAnnotations;

namespace ECommerceAPI.DTOs.Request.Category
{
    /// <summary>
    /// Represents a request object for creating new Category.
    /// </summary>
    public class NewCategoryDto
    {
        /// <summary>
        /// New Category Name.
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// New Category Codename.
        /// </summary>
        [Required]
        public string Codename { get; set; }
    }
}
