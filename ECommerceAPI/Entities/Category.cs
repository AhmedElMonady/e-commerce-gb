﻿namespace ECommerceAPI.Entities
{
    /// <summary>
    /// Represents a Category entity in the database.
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Unique Category Id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Unique Category Name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Category Codename.
        /// </summary>
        public string Codename { get; set; }
        /// <summary>
        /// List of Category Products.
        /// </summary>
        public List<Product> Products { get; set; }
    }
}
