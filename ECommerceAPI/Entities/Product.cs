﻿namespace ECommerceAPI.Entities
{
    /// <summary>
    /// Represents a Product entity in the database.
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Unique Product Id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Unique Product Name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Product price.
        /// </summary>
        public double Price { get; set; }
        /// <summary>
        /// Product description.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Custom Generated ProductId
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// Product Category.
        /// </summary>
        public Category ProductCategory { get; set; }
        /// <summary>
        /// Product Category Id to be referenced with.
        /// </summary>
        public int CategoryId { get; set; }
    }
}
