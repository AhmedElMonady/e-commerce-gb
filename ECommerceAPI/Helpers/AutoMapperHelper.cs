﻿using AutoMapper;
using ECommerceAPI.DTOs.Response;
using ECommerceAPI.Entities;

namespace ECommerceAPI.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            //Maps from Product to ProductResponse class
            CreateMap<Product, ProductResponse>()
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price.ToString("0.00")));
            //Maps from Category to CategoryResponse class
            CreateMap<Category, CategoryResponse>();
        }
    }
}
