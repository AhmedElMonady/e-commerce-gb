﻿namespace ECommerceAPI.Interfaces
{
    /// <summary>
    /// Represents the Unit of Work pattern collecting all repositories and data related operations.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// An instance of Categories Repository.
        /// </summary>
        ICategoriesRepository CategoriesRepository { get; }

        /// <summary>
        /// An instance of Products Repository.
        /// </summary>
        IProductsRepository ProductsRepository { get; }

        /// <summary>
        /// Checks whether a change has happened or not when saving the context.
        /// </summary>
        /// <returns>True if a change is saved, False otherwise.</returns>
        Task<bool> CheckSaveStatusAsync();
    }
}
