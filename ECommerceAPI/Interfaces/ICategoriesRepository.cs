﻿using ECommerceAPI.DTOs.Request.Category;
using ECommerceAPI.DTOs.Response;
using ECommerceAPI.Entities;

namespace ECommerceAPI.Interfaces
{
    /// <summary>
    /// Represents the Categpry Repository that holds all data driven operations.
    /// </summary>
    public interface ICategoriesRepository
    {
        /// <summary>
        /// Gets All Categories from the database.
        /// </summary>
        /// <returns>List of the retieved Categories.</returns>
        Task<List<CategoryResponse>> GetAllCategoriesAsync();

        /// <summary>
        /// Adds new Category item in the database.
        /// </summary>
        /// <param name="newCategoryDto">The new Category name and codename.</param>
        /// <returns></returns>
        Task AddNewCategoryAsync(NewCategoryDto newCategoryDto);

        /// <summary>
        /// Checks for whether a Category already exists with the same given name or not.
        /// </summary>
        /// <param name="categoryName">Name of the to-be-added Category.</param>
        /// <returns>True if no Category exist with that name otherwise False.</returns>
        Task<bool> CheckCategoryNameExistsAsync(string categoryName);
        
        /// <summary>
        /// Gets a Category by its name.
        /// </summary>
        /// <param name="categoryName">Category name to be retrieved.</param>
        /// <returns>The Category retrieved if existed.</returns>
        Task<Category> GetCategoryByNameAsync(string categoryName);
    }
}
