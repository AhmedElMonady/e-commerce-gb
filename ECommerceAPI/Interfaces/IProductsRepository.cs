﻿using ECommerceAPI.DTOs.Request.Product;
using ECommerceAPI.DTOs.Response;
using ECommerceAPI.Entities;

namespace ECommerceAPI.Interfaces
{
    /// <summary>
    /// Represents the Product Repository that holds all data driven operations.
    /// </summary>
    public interface IProductsRepository
    {
        /// <summary>
        /// Adds new Product item in the database.
        /// </summary>
        /// <param name="newProductDto">Holds the new product details.</param>
        /// <param name="category">The Category that the product will be linked to.</param>
        /// <returns></returns>
        Task AddNewProductAsync(NewProductDto newProductDto, Category category);

        /// <summary>
        /// Checks for whether a Product already exists with the same given name or not.
        /// </summary>
        /// <param name="productName">Name of the to-be-added Product.</param>
        /// <returns>True if no Product exist with that name otherwise False.</returns>
        Task<bool> CheckProductNameExistsAsync(string productName);

        /// <summary>
        /// Gets the Products that are linked to a specific Category.
        /// </summary>
        /// <param name="getCategoryProductsDto">Contains the Category name and the sort order required.</param>
        /// <returns>A list of Category-related Products.</returns>
        Task<List<ProductResponse>> CategoryProductsAsync(GetCategoryProductsDto getCategoryProductsDto);
    }
}
