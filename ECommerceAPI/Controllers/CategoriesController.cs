﻿using ECommerceAPI.DTOs.Request.Category;
using ECommerceAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ECommerceAPI.Controllers
{
    public class CategoriesController : BaseApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public CategoriesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Gets all Categories stored in the database.
        /// </summary>
        /// <returns>List of retrieved Categories.</returns>
        /// <response code="200">Returns Ok: List of Categories retrieved.</response>
        /// <response code="404">Returns NotFound:"No categories present".</response>
        [HttpGet("All")]
        public async Task<ActionResult> GetAllCategories()
        {
            var categories =  await _unitOfWork.CategoriesRepository.GetAllCategoriesAsync();

            if (categories == null || !categories.Any()) 
                return NotFound("No categories present");

            return Ok(categories);
        }

        /// <summary>
        /// Adds New Category to the database if it does not exist.
        /// </summary>
        /// <param name="newCategoryDto">Category name and codename.</param>
        /// <returns></returns>
        /// <response code="200">Returns OK: "Category added successfully".</response>
        /// <response code="400">Returns BadRequest: "Category already exist" OR "Failed to add new Category".</response>
        [HttpPost("New")]
        public async Task<ActionResult> AddNewCategory(NewCategoryDto newCategoryDto)
        {
            var check = await _unitOfWork.CategoriesRepository.CheckCategoryNameExistsAsync(newCategoryDto.Name);

            if (!check) 
                return BadRequest("Category already exist");

            await _unitOfWork.CategoriesRepository.AddNewCategoryAsync(newCategoryDto);

            var status = await _unitOfWork.CheckSaveStatusAsync();
            
            if (!status) return BadRequest("Failed to add new Category");

            return Ok("Category added successfully");
        }
    }
}
