﻿using ECommerceAPI.DTOs.Request.Product;
using ECommerceAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ECommerceAPI.Controllers
{
    public class ProductsController : BaseApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Adds new Product to the database if not exists.
        /// </summary>
        /// <param name="newProductDto">New Product details.</param>
        /// <returns></returns>
        /// <response code="200">Returns Ok: "Product added successfully".</response>
        /// <response code="400">Returns BadRequest: "Product already exist" OR "Category does not exist" OR "Failed to add Product".</response>
        [HttpPost("New")]
        public async Task<ActionResult> AddNewProduct(NewProductDto newProductDto)
        {
            var check = await _unitOfWork.ProductsRepository.CheckProductNameExistsAsync(newProductDto.Name);

            if (!check)
                return BadRequest("Product already exist");

            var category = await _unitOfWork.CategoriesRepository.GetCategoryByNameAsync(newProductDto.CategoryName);
            
            if (category == null) 
                return BadRequest("Category does not exist");

            await _unitOfWork.ProductsRepository.AddNewProductAsync(newProductDto, category);

            var status = await _unitOfWork.CheckSaveStatusAsync();

            if (!status) return BadRequest("Failed to add Product");

            return Ok("Product added successfully");
        }

        /// <summary>
        /// Gets Category-related Products if exist.
        /// </summary>
        /// <param name="getCategoryProductsDto">Category name and Sorting order.</param>
        /// <returns>List of Category-related Products.</returns>
        /// <response code="200">Returns Ok: List of Category-related Products retrieved.</response>
        /// <response code="400">Returns BadRequest: "No such products in this category or product doesnot exist".</response>
        [HttpPost("CategoryProducts")]
        public async Task<ActionResult> GetCategoryProducts(GetCategoryProductsDto getCategoryProductsDto)
        {
            var categoryProducts = await _unitOfWork.ProductsRepository.CategoryProductsAsync(getCategoryProductsDto);
            
            if (!categoryProducts.Any())
                return BadRequest("No such products in this category or product doesnot exist");

            return Ok(categoryProducts);
        }
    }
}
